var ws = require('nodejs-websocket');

var server = ws.createServer(function (conn) {
  // 每当接收到用户传递过来的数据(客户端的send方法)，text事件就会被触发
  conn.on('text', function (str) {
    var data = JSON.parse(str);
    console.log(data);

    switch (data.type) {
      case 'setname':
        conn.nickname = data.name;

        boardcast(JSON.stringify({
          type: 'serverInformation',
          message: data.name + ' 加入房间'
        }));

        boardcast(JSON.stringify({
          type: 'chatterList',
          list: getAllChatter()
        }))
        break;

      case 'chat':
        boardcast(JSON.stringify({
          type: 'chat',
          name: conn.nickname,
          message: data.message
        }));
        break;

      default:
        break;
    }
  });

  // 只要 websocket 连接断开，close事件就会触发
  conn.on('close', function () {
    boardcast(JSON.stringify({
      type: 'serverInformation',
      message: conn.nickname + ' 离开房间'
    }));

    boardcast(JSON.stringify({
      type: 'chatterList',
      list: getAllChatter()
    }))
  });

  // error 必须写出来，否则每当客户端关闭时，后端服务器会崩溃自动关闭。
  conn.on('error', function (err) {
    console.log(err);
  });
})
server.listen(8000, () => {
  console.log('Server started Complete ws://localhost:8000');
})

function boardcast(str) {
  server.connections.forEach(function (conn) {
    conn.sendText(str);
  })
}

function getAllChatter() {

  var chatterArr = [];

  server.connections.forEach(function (conn) {
    chatterArr.push({ name: conn.nickname });
  })

  return JSON.stringify(chatterArr);
}