## 一、项目介绍

该项目是一个基于 WebSocket 实现的在线聊天室

项目预览：
![用户一](https://img-blog.csdn.net/20180512185537844?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI2ODIyMDI5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

![用户二](https://img-blog.csdn.net/2018051218560290?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI2ODIyMDI5/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

## 二、如何运行项目

### 2.1 安装依赖

当你 clone 该项目后，进入项目的根目录 运行 npm install，安装项目依赖

### 2.2 启动服务器

在根目录下，输入 `node .\app.js`  启动服务器。

### 2.3 把 index.html 运行到浏览器

接下来 把 `index.html` 文件运行到浏览器，你就拥有了一个在线聊天室.