## 介绍

该项目是使用 socket.io库实现的一个仿微信聊天室

部分预览
![登录](./public/images/readme/login.png)

![预览1](./public/images/readme/1.png)

![预览2](./public/images/readme/2.png)

![预览3](./public/images/readme/3.png)

![预览4](./public/images/readme/4.png)


## 如何运行该项目？

当你 clone 项目后，你首先 应该 `npm install`,安装依赖，安装过后。会在你本地 `http://127.0.0.1:8080` 开启一个服务，访问该页面即可预览项目。