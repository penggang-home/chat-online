// 1.导入 Nodejs-websocket 依赖
const ws = require("nodejs-websocket")

// 2.创建一个 server

// 每次只要有用户连接，函数就会被执行，就会给当前用户创建一个 connect对象
const server = ws.createServer(connect => {
  console.log('有新用户连接上来！');

  // 每当接收到用户传递过来的数据(客户端的send方法)，text事件就会被触发
  connect.on('text', data => {
    console.log('接收到了用户数据：' + data);
    // 给用户返回数据,对用户发送过来的数据小写转化为大写
    connect.send(data.toUpperCase())
  })

  // 只要 websocket 连接断开，close事件就会触发
  connect.on('close', (event) => {
    console.log('有用户断开连接：' + event);
  })

  // 注册一个 error,处理用户的错误信息
  connect.on('error', (event) => {
    console.log('异常处理:' + event);
  })
})

server.listen(3000, () => {
  console.log('Server started Complete ws://localhost:3000');
})